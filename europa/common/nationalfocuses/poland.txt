### TO CREATE ###
# event to move capital to warsaw (if agreed to), add max infrastrucute and 4 building slots(2/2)
# poland.1 - 01 = annex, 02 = annex_wargoal
# poland.2 - same as poland.1
# poland.3 - 01 = vassal LIT, 02 = ally vassal(-150pp)
# poland.4 - create commonwealth

focus_tree = {

	focus = {			#PP
		id = POL_PP
		icon = GFX_goal_generic_allies_build_infantry
		x = 3
		y = 0
		cost = 4

		available_if_capitulated = yes

		completion_reward = {
				add_political_power = 150
			}
		}
	}

	focus = {			#annex mazovia
		id = POL_MAZ
		icon = GFX_goal_generic_allies_build_infantry
		x = 1
		y = 1
		cost = 10

		available_if_capitulated = no
		prerequisite = { focus = POL_PP}

		available = {
			country_exists = MAZ
			is_puppet = no
			MAZ = {
				is_puppet = yes							#check
			}
		}
		cancel = {
			is_puppet = yes
			MAZ = {
				is_puppet = no							#check
			}
		}
		bypass = {
			MAZ = {
				exists = no
			}
		}

		completion_reward = {
				army_experience = 5
				add_political_power = -80
				MAZ = {									#check
					country_event = poland.1
				}
#				add_state_core = 0
#				add_state_core = 0
		}
	}

	focus = {			#annex moldavia
		id = POL_MOL
		icon = GFX_goal_generic_allies_build_infantry
		x = 2
		y = 1
		cost = 10

		available_if_capitulated = no
		prerequisite = { foucs = POL_PP }

		available = {
			is_puppet = no
			country_exists = MOL
			MOL = {
				is_puppet = yes							#check
			}
		}
		cancel = {
			is_puppet = yes
			MOL = {
				is_puppet = no							#check
			}
		}
		bypass = {
			MAZ = {
				exists = no
			}
		}

		completion_reward = {
			army_experience = 5
			add_political_power = -80
			MOL = {										#check
				country_event = poland.2
			}
#			add_state_core = 0
#			add_state_core = 0
		}
	}

	focus = {			#LIT(vassal||ally)
		id = POL_LIT
		icon = GFX_goal_generic_allies_build_infantry
		x = 4
		y = 1
		cost = 10

		available_if_capitulated = no
		prerequisite = { focus = POL_PP }

		available = {
			is_puppet = no
			country_exists = LIT
		}

		completion_reward = {
				country_event = poland.3				#check
				add_political_power = -50
			}
	}

	focus = {			#attack_teutons
		id = POL_TTOATTACK
		icon = GFX_goal_generic_allies_build_infantry
		x = 3
		y = 2
		cost = 7

		available_if_capitulated = no

		available = {
			has_army_manpower = {size > 30000 } #30 div?
		}

		completion_reward = {
				create_wargoal = {
					type = annex_everything
					target = TTO
				}
		}
	}

	focus = {			#core_teutons
		id = POL_TTOCORE
		icon = GFX_goal_generic_allies_build_infantry
		x = 3
		y = 3
		cost = 5

		available_if_capitulated = yes
		prerequisite = { focus = POL_TTOATTACK }		

		completion_reward = {
#				add_state_core = 0
#				add_state_core = 0
		}
	}

	focus = {			#claim_silesia
		id = POL_CLAIMSILESIA
		icon = GFX_goal_generic_allies_build_infantry
		x = 2
		y = 4
		cost = 6

		available_if_capitulated = yes
		
		available = {
			has_army_manpower = { size > 50000 } #50 div?
		}	

		completion_reward = {
#				0 = { add_claim_by = POL}
#				0 = { add_claim_by = POL}
		}
	}

	focus = {			#attack_silesia
		id = POL_ATTACKSILESIA
		icon = GFX_goal_generic_allies_build_infantry
		x = 2
		y = 5
		cost = 8

		available_if_capitulated = no
		prerequisite = { focus = POL_CLAIMSILESIA }		

		completion_reward = {
				create_wargoal = {
					type = take_state_focus
					target = SIL
					generator = { 0 }					#check
				}
		}
	}

		focus = {			#claim_baltics
		id = POL_CLAIMBALTICS
		icon = GFX_goal_generic_allies_build_infantry
		x = 1
		y = 6
		cost = 8

		available_if_capitulated = yes
		
		available = {
			has_army_manpower = { size > 35000 } #35 div?
		}	

		completion_reward = {
#			add_state_core = 0
#			add_state_core = 0
#			add_state_core = 0
				create_wargoal = {
					type = annex_everything
					target = LIV
					target = RIG
				}
		}
	}

	focus = {			#form_commonwealth
		id = POL_COMMONWEALTH
		icon = GFX_goal_generic_allies_build_infantry
		x = 8
		y = 0
		cost = 10

		available_if_capitulated = no	

		available = {
			is_puppet = no
			MAZ = { is_puppet = yes }
			MOL = { is_puppet = yes }
			LIT = { is_puppet = yes }
#			date > 0000.0.0
		}

		completion_reward = {
			country_event = poland.4
		}
	}
}