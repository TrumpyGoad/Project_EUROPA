# All infantry equipment

equipments = {

	#PIKES#

	pike_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_pike_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 4

		#Defensive Abilities
		defense = 18
		breakthrough = 1.6
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 1.5
		hard_attack = 3
		ap_attack = 1
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.4
		resources = {
			steel = 1
			chromium = 2
		}
		
	}
	
	pike_equipment_0 = {
		year = 1300
	
		archetype = pike_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#pike_equipment_1 = {
	#	year = 1440
	#
	#	archetype = pike_equipment
	#	parent = pike_equipment_0
	#	priority = 10
	#	visual_level = 1
	#
	#	#Defensive Abilities
	#	defense = 22
	#	breakthrough = 1.5
	#
	#	#Offensive Abilities
	#	soft_attack = 2.5
	#	hard_attack = 2
	#	ap_attack = 1
	#	air_attack = 0
	#
	#	build_cost_ic = 0.5
	#}
	
	#HALBIERDS#
	
	halberd_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_halberd_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 4

		#Defensive Abilities
		defense = 16
		breakthrough = 2
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 2
		hard_attack = 2.5
		ap_attack = 0.8
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.6
		resources = {
			steel = 2
			chromium = 1
		}
		
	}
	
	halberd_equipment_0 = {
		year = 1300
	
		archetype = halberd_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#BOWS#
	
	bow_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_bow_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 4

		#Defensive Abilities
		defense = 6
		breakthrough = 0.4
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 5
		hard_attack = 2
		ap_attack = 1.4
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.4
		resources = {
			chromium = 2
			aluminium = 1
		}
		
	}
	
	bow_equipment_0 = {
		year = 1300
	
		archetype = bow_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#ARBALESTS#
	
	arbalest_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_arbalest_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.8
		maximum_speed = 4

		#Defensive Abilities
		defense = 10
		breakthrough = 0.8
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 8
		hard_attack = 1
		ap_attack = 1.2
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 1.2
		resources = {
			chromium = 2
			aluminium = 1
			steel = 1
		}
		
	}
	
	arbalest_equipment_0 = {
		year = 1300
	
		archetype = arbalest_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#INFANTRY SWORDS#
	
	infantry_sword_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_infantry_sword_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 4

		#Defensive Abilities
		defense = 14
		breakthrough = 1.2
		hardness = 0.5
		armor_value = 0.5

		#Offensive Abilities
		soft_attack = 3
		hard_attack = 1
		ap_attack = 0.6
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.8
		resources = {
			steel = 2
			chromium = 1
		}
		
	}
	
	infantry_sword_equipment_0 = {
		year = 1300
	
		archetype = infantry_sword_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#LIGHT ATTIRE#

	light_attire_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_light_attire_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 6

		#Defensive Abilities
		defense = 12
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 0
		hard_attack = 0
		ap_attack = 0
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.4
		resources = {
			steel = 1
			aluminium = 3
		}
		
	}
	
	light_attire_equipment_0 = {
		year = 1300
	
		archetype = light_attire_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#HEAVY ATTIRE#
	
	heavy_attire_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_heavy_attire_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 6

		#Defensive Abilities
		defense = 20
		breakthrough = 0
		hardness = 0.8
		armor_value = 1

		#Offensive Abilities
		soft_attack = 0
		hard_attack = 0
		ap_attack = 0
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 1
		resources = {
			steel = 3
			aluminium = 1
		}
		
	}
	
	heavy_attire_equipment_0 = {
		year = 1300
	
		archetype = heavy_attire_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#ARQUEBUS#
	
	arquebus_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_arquebus_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.75
		maximum_speed = 4

		#Defensive Abilities
		defense = 6
		breakthrough = 1
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 10
		hard_attack = 2.5
		ap_attack = 1.6
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 1.6
		resources = {
			steel = 1
			chromium = 2
			rubber = 1
		}
		
	}
	
	arquebus_equipment_0 = {
		year = 1300
	
		archetype = arquebus_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#WAR HORSE#
	
	war_horse = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_war_horse		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.8
		maximum_speed = 6

		#Defensive Abilities
		defense = 20
		breakthrough = 1
		hardness = 0.3
		armor_value = 1

		#Offensive Abilities
		soft_attack = 0
		hard_attack = 0
		ap_attack = 0
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 1
		resources = {
			oil = 2
			aluminium=1
			steel = 1
		}
		
	}
	
	war_horse_0 = {
		year = 1300
	
		archetype = war_horse
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#CAV LANCE#
	
	lance_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_lance_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 6

		#Defensive Abilities
		defense = 0
		breakthrough = 2
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 8
		hard_attack = 2
		ap_attack = 2
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.6
		resources = {
			chromium = 2
			steel = 2
		}
		
	}
	
	lance_equipment_0 = {
		year = 1300
	
		archetype = lance_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#FIELD GUN#
	
	field_gun_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_field_gun_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.8
		maximum_speed = 2

		#Defensive Abilities
		defense = 6
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 24
		hard_attack = 2
		ap_attack = 1.6
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 16
		resources = {
			tungsten = 4
			chromium = 4
			rubber = 2
		}
		
	}
	
	field_gun_equipment_0 = {
		year = 1300
	
		archetype = field_gun_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#MORTAR#
	
	mortar_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_mortar_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.8
		maximum_speed = 2

		#Defensive Abilities
		defense = 4
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 20
		hard_attack = 4
		ap_attack = 2.4
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 20
		resources = {
			tungsten = 5
			chromium = 4
			rubber = 3
		}
		
	}
	
	mortar_equipment_0 = {
		year = 1300
	
		archetype = mortar_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#REGIMENTAL GUN#
	
	regimental_gun_equipment = {
		year = 1300
	
		is_archetype = yes
		picture = archetype_regimental_gun_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.8
		maximum_speed = 4

		#Defensive Abilities
		defense = 8
		breakthrough = 0
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 16
		hard_attack = 1
		ap_attack = 1
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 12
		resources = {
			tungsten = 3
			chromium = 3
			rubber = 2
		}
		
	}
	
	regimental_gun_equipment_0 = {
		year = 1300
	
		archetype = regimental_gun_equipment
		active = yes
		priority = 05
		visual_level = 0
	}
	
	#BASE GAME SHIT#
	
	infantry_equipment = {
		year = 1936
	
		is_archetype = yes
		picture = archetype_infantry_equipment		
		is_buildable = no
		type = infantry
		group_by = archetype
		
		interface_category = interface_category_land
		
		active = yes
		
		#Misc Abilities
		reliability = 0.9
		maximum_speed = 4

		#Defensive Abilities
		defense = 20
		breakthrough = 2
		hardness = 0
		armor_value = 0

		#Offensive Abilities
		soft_attack = 3
		hard_attack = 0.5
		ap_attack = 1
		air_attack = 0

		#Space taken in convoy
		lend_lease_cost = 1
		
		build_cost_ic = 0.4
		resources = {
			steel = 2
		}
		
	}

	infantry_equipment_0 = {
		year = 1918
	
		archetype = infantry_equipment
		active = yes
		priority = 05
		visual_level = 0
	}



	# Regular infantry  1936
	infantry_equipment_1 = {
		year = 1936
	
		archetype = infantry_equipment
		parent = infantry_equipment_0
		priority = 10
		visual_level = 1

		#Defensive Abilities
		defense = 22
		breakthrough = 3

		#Offensive Abilities
		soft_attack = 6
		hard_attack = 1
		ap_attack = 4
		air_attack = 0

		build_cost_ic = 0.5
	}

	# Improved weapons  ~1939
	infantry_equipment_2 = {
		year = 1939
	
		archetype = infantry_equipment
		parent = infantry_equipment_1
		priority = 10
		visual_level = 2
		
		#Defensive Abilities
		defense = 28
		breakthrough = 4

		#Offensive Abilities
		soft_attack = 9
		hard_attack = 1.5
		ap_attack = 5
		air_attack = 0
		
		build_cost_ic = 0.6
		resources = {
			steel = 3
		}
	}
	
	# Advanced weapons  ~1942
	infantry_equipment_3 = {
		year = 1942
	
		archetype = infantry_equipment
		parent = infantry_equipment_2
		priority = 10
		visual_level = 3
		
		#Defensive Abilities
		defense = 34
		breakthrough = 5

		#Offensive Abilities
		soft_attack = 12
		hard_attack = 2
		ap_attack = 10
		air_attack = 0
		
		reliability = 0.8
		build_cost_ic = 0.7
		resources = {
			steel = 4
		}
	}
}
