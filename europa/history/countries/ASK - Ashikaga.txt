﻿capital = 893

oob = "ASK_1444"

set_research_slots = 3

add_ideas = {
	limited_conscription
}
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_mountaineers = 1
	tech_support = 1		
	tech_recon = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	transport = 1
}

set_convoys = 50

set_politics = {

	parties = {
		neutrality={
			popularity=0
		}
		protestant={
			popularity=0
		}
		reformed={
			popularity=0
		}
		orthodox={
			popularity=0
		}
		coptic={
			popularity=0
		}
		anglican={
			popularity=0
		}
		fascism={
			popularity=0
		}
		shia={
			popularity=0
		}
		ibadi={
			popularity=0
		}
		theravada={
			popularity=0
		}
		vajrayana={
			popularity=0
		}
		mahayana={
			popularity=0
		}
		confucian={
			popularity=0
		}
		shinto={
			popularity=100
		}
		hindu={
			popularity=0
		}
		sikh={
			popularity=0
		}
		communism={
			popularity=0
		}
		fetishist={
			popularity=0
		}
		totemist={
			popularity=0
		}
		inti={
			popularity=0
		}
		nahuatl={
			popularity=0
		}
		mayan={
			popularity=0
		}
		tengri={
			popularity=0
		}
		norse={
			popularity=0
		}
		democratic={
			popularity=0
		}
		zoroastrian={
			popularity=0
		}
	}
	
	ruling_party = shinto
	last_election = "1443.8.16"
	election_frequency = 48
	elections_allowed = no
}



create_country_leader = {
	name = "Shogun Yoshimasa"
	desc = ""
	picture = "Yoshimasa_Ashikaga.dds"
	expire = "9999.1.1"
	ideology = snt
	traits = {
		#
	}
}

give_guarantee = AMA
give_guarantee = DTE
give_guarantee = HSK
give_guarantee = HTK
give_guarantee = IMG
give_guarantee = ODA
give_guarantee = OTM
give_guarantee = OUC
give_guarantee = SBA
give_guarantee = SMZ
give_guarantee = TKD
give_guarantee = TKG
give_guarantee = UES
give_guarantee = YMN
give_guarantee = NNB
give_guarantee = KTB
give_guarantee = ADO
give_guarantee = CBA
give_guarantee = ISK
give_guarantee = ITO
give_guarantee = KKC
give_guarantee = KNO
give_guarantee = OGS
give_guarantee = SHN
give_guarantee = STK
give_guarantee = TKI
give_guarantee = UTN
give_guarantee = TTI
