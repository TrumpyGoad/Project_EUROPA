﻿capital = 43

oob = "HUN_1444"

set_research_slots = 3

add_ideas = {
	limited_conscription
}
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_mountaineers = 1
	tech_support = 1		
	tech_recon = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	transport = 1
}

set_convoys = 50

1439.10.27={
	set_politics = {

	parties = {
		neutrality={
			popularity=77.7
		}
		protestant={
			popularity=0
		}
		reformed={
			popularity=0
		}
		orthodox={
			popularity=22.3
		}
		coptic={
			popularity=0
		}
		anglican={
			popularity=0
		}
		fascism={
			popularity=0
		}
		shia={
			popularity=0
		}
		ibadi={
			popularity=0
		}
		theravada={
			popularity=0
		}
		vajrayana={
			popularity=0
		}
		mahayana={
			popularity=0
		}
		confucian={
			popularity=0
		}
		shinto={
			popularity=0
		}
		hindu={
			popularity=0
		}
		sikh={
			popularity=0
		}
		communism={
			popularity=0
		}
		fetishist={
			popularity=0
		}
		totemist={
			popularity=0
		}
		inti={
			popularity=0
		}
		nahuatl={
			popularity=0
		}
		mayan={
			popularity=0
		}
		tengri={
			popularity=0
		}
		norse={
			popularity=0
		}
		democratic={
			popularity=0
		}
		zoroastrian={
			popularity=0
		}
	}
	
	ruling_party = neutrality
	last_election = "1439.10.27"
	election_frequency = 48
	elections_allowed = no
	}

	create_country_leader = {
		name = "~Interregnum~"
		desc = ""
		picture = "HUN_Council.dds"
		expire = "9999.1.1"
		ideology = cat
		traits = {
		#
		}
	}
}

1444.11.11={
	set_politics = {

	parties = {
		neutrality={
			popularity=77.7
		}
		protestant={
			popularity=0
		}
		reformed={
			popularity=0
		}
		orthodox={
			popularity=22.3
		}
		coptic={
			popularity=0
		}
		anglican={
			popularity=0
		}
		fascism={
			popularity=0
		}
		shia={
			popularity=0
		}
		ibadi={
			popularity=0
		}
		theravada={
			popularity=0
		}
		vajrayana={
			popularity=0
		}
		mahayana={
			popularity=0
		}
		confucian={
			popularity=0
		}
		shinto={
			popularity=0
		}
		hindu={
			popularity=0
		}
		sikh={
			popularity=0
		}
		communism={
			popularity=0
		}
		fetishist={
			popularity=0
		}
		totemist={
			popularity=0
		}
		inti={
			popularity=0
		}
		nahuatl={
			popularity=0
		}
		mayan={
			popularity=0
		}
		tengri={
			popularity=0
		}
		norse={
			popularity=0
		}
		democratic={
			popularity=0
		}
		zoroastrian={
			popularity=0
		}
	}
	
	ruling_party = neutrality
	last_election = "1439.10.27"
	election_frequency = 48
	elections_allowed = no
	}
	
	create_country_leader = {
		name = "~Interregnum~"
		desc = ""
		picture = "HUN_Council.dds"
		expire = "9999.1.1"
		ideology = cat
		traits = {
			#
		}
	}
}

1453.5.29={
	set_politics = {

	parties = {
		neutrality={
			popularity=77.7
		}
		protestant={
			popularity=0
		}
		reformed={
			popularity=0
		}
		orthodox={
			popularity=22.3
		}
		coptic={
			popularity=0
		}
		anglican={
			popularity=0
		}
		fascism={
			popularity=0
		}
		shia={
			popularity=0
		}
		ibadi={
			popularity=0
		}
		theravada={
			popularity=0
		}
		vajrayana={
			popularity=0
		}
		mahayana={
			popularity=0
		}
		confucian={
			popularity=0
		}
		shinto={
			popularity=0
		}
		hindu={
			popularity=0
		}
		sikh={
			popularity=0
		}
		communism={
			popularity=0
		}
		fetishist={
			popularity=0
		}
		totemist={
			popularity=0
		}
		inti={
			popularity=0
		}
		nahuatl={
			popularity=0
		}
		mayan={
			popularity=0
		}
		tengri={
			popularity=0
		}
		norse={
			popularity=0
		}
		democratic={
			popularity=0
		}
		zoroastrian={
			popularity=0
		}
	}
	
	ruling_party = neutrality
	last_election = "1439.10.27"
	election_frequency = 48
	elections_allowed = no
	}

	create_country_leader = {
		name = "John Hunyadi"
		desc = ""
		picture = "John_Hunyadi.dds"
		expire = "9999.1.1"
		ideology = cat
		traits = {
		#
		}
	}
}