
state={
	id=4
	name="STATE_4"
	resources={
		oil=18
	}

	history={
		owner = AUS
		victory_points = {
			11666 20 
		}
		victory_points = {
			6723 15 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 2
		}
		add_core_of = AUS

	}

	provinces={
		557 704 3718 6552 6723 6739 9527 11525 11651 11666 
	}
	manpower=650264
	buildings_max_level_factor=1.000
	state_category=large_city
}
