state={
	id=148
	name="STATE_148"
	resources={
		chromium=10
	}

	history={
		owner = SWE
		victory_points = {
			6087 1 
		}
		victory_points = {
			3107 1 
		}
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			11164 = {
				naval_base = 1
			}
		}
		add_core_of = FIN
		add_core_of = SWE
	}

	provinces={
		30 103 124 162 167 203 2992 3026 3055 3069 3097 3107 3153 3156 3158 3193 6045 6061 6076 6087 6128 6137 6142 6179 9112 9189 9223 11030 11115 11125 11130 11164 11167 11204 
	}
	manpower=61364
	buildings_max_level_factor=1.000
	state_category=rural
}
