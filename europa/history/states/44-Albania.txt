
state={
	id=44
	name="STATE_44"
	resources={
		oil=4
		aluminium=10
	}

	history={
		owner = ALB
		victory_points = {
			9780 3 
		}
		buildings = {
			infrastructure = 2
			9780 = {
				naval_base = 1

			}

		}
		add_core_of = ALB
		add_core_of = TUR

	}

	provinces={
		3884 3896 9780 9875 9914 11762 
	}
	manpower=126984
	buildings_max_level_factor=1.000
	state_category=rural
}
