
state={
	id=461
	name="STATE_461"
	resources={
		tungsten=30
	}

	history={
		owner = MOR
		add_core_of = MOR
		victory_points = {
			7069 3 
		}
		victory_points = {
			10070 10 
		}
		victory_points = {
			4084 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 1
			dockyard = 1
			7069 = {
				naval_base = 1

			}
			10070 = {
				naval_base = 2

			}

		}

	}

	provinces={
		1079 4084 7069 10023 10070 
	}
	manpower=429402
	buildings_max_level_factor=1.000
	state_category=city
}
