
state={
	id=54
	name="STATE_54"
	resources={
		oil=4
		chromium=8
	}

	history={
		owner = WBG
		buildings = {
			infrastructure = 3
			industrial_complex = 1

		}
		victory_points = {
			3474 3 
		}
		victory_points = {
			11417 5 
		}
		add_core_of = WBG

	}

	provinces={
		6421 9416 9557 9572 11404 11417 11445 13116 
	}
	manpower=176879
	buildings_max_level_factor=1.000
	state_category=town
}
