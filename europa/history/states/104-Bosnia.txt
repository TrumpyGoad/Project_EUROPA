
state={
	id=104
	name="STATE_104"
	resources={
		chromium=6
	}

	history={
		owner = BOS
		victory_points = {
			11574 5 
		}
		buildings = {
			infrastructure = 2
		}
		add_core_of = BOS
	}

	provinces={
		606 6799 9588 9591 11572 11574 
	}
	manpower=123023
	buildings_max_level_factor=1.000
	state_category=rural
}
