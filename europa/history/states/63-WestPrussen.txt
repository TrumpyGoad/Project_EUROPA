
state={
	id=63
	name="STATE_63"
	resources={
		chromium=20
	}

	history={
		owner = POM
		victory_points = {
			11316 3
		}
		victory_points = {
			9361 5
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			11372 = {
				naval_base = 1
			}
			9361 = {
				naval_base = 2
			}
		}
		add_core_of = POM
	}

	provinces={
		9361 11288 11316 11343 11372 
	}
	manpower=206358
	buildings_max_level_factor=1.000
	state_category=town
}
