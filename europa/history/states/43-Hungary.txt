
state={
	id=43
	name="STATE_43"
	resources={
		oil=16
	}

	history={
		owner = HUN
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 2
		}
		victory_points = {
			9660 10 
		}
		victory_points = {
			3731 5 
		}
		add_core_of = HUN
	}

	provinces={
		684 716 3713 3731 6716 6751 9660 9690 11520 
	}
	manpower=474517
	buildings_max_level_factor=1.000
	state_category=city
}
