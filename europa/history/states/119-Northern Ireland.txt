
state={
	id=119
	name="STATE_119"
	resources={
		chromium=12
	}

	history={
		owner = TYR
		victory_points = {
			3379 5 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			3379 = {
				naval_base = 2

			}

		}
		#add_core_of = ULS

	}

	provinces={
		3217 3329 3379 
	}
	manpower=65882
	buildings_max_level_factor=1.000
	state_category=rural
}
