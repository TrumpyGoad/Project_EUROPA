state={
	id=970
	name="STATE_970"
	resources={
		oil=10
	}
	
	history={
		owner = ENG
		victory_points = {
			11548 15 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			11548 = {
				naval_base = 3
			}
		}
		add_core_of = ENG
	}
	
	provinces={
		11548 
	}
	manpower=228643
	state_category=town
	buildings_max_level_factor=1.000
}
