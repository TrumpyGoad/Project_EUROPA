
state={
	id=83
	name="STATE_83"
	manpower = 140598
	resources={
		oil=4
	}
	
	state_category = rural

	history={
		owner = HUN
		buildings = {
			infrastructure = 2
			industrial_complex = 1
		}
		victory_points = {
			9640 3 
		}
		add_core_of = HUN
		add_core_of = TRA
	}

	provinces={
		696 6682 6697 9640 
	}
}
