
state={
	id=26
	name="STATE_26"
	resources={
		steel=40
	}

	history={
		owner = FRA
		victory_points = {
			590 10 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = FRA
		add_core_of = AUV
	}

	provinces={
		590 769 3754 6773 6789 9593 9748 
	}
	manpower=193467
	buildings_max_level_factor=1.000
	state_category=town
}
