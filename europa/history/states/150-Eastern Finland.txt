state={
	id=150
	name="STATE_150"
	resources={
		chromium=10
	}

	history={
		owner = SWE
		victory_points = {
			3149 1 
		}
		victory_points = {
			37 1 
		}
		buildings = {
			infrastructure = 2
		}
		add_core_of = FIN
		add_core_of = SWE
	}

	provinces={
		37 136 141 165 3013 3073 3115 3125 3131 3149 6033 6078 6134 6146 6156 9056 9104 9106 9163 9233 11024 11043 11076 11079 11141 11199 11210 13104 13105 13108 13109 13110 13111 13112 13118 
	}
	manpower=40909
	buildings_max_level_factor=1.000
	state_category=rural
}
