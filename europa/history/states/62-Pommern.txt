
state={
	id=62
	name="STATE_62"
	resources={
		oil=4
		chromium=28
	}

	history={
		owner = POM
		victory_points = {
			6282 10 
		}
		victory_points = {
			3340 3 
		}
		victory_points = {
			9388 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			arms_factory = 2
			dockyard = 1
			6334 = {
				naval_base = 2

			}
			3340 = {
				naval_base = 1

			}
			9388 = {
				naval_base = 2

			}

		}
		add_core_of = POM

	}

	provinces={
		349 3340 6282 6334 6390 9334 9388 
	}
	manpower=353757
	buildings_max_level_factor=1.000
	state_category=city
}
