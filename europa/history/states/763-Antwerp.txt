state={
	id=763
	name="STATE_763"
	resources={
		oil=38
		aluminium=35
	}

	history={
		owner = BRB
		victory_points = {
			6598 20 
		}
		victory_points = {
			516 20 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			arms_factory = 1
			dockyard = 1
			6598 = {
				naval_base = 4

			}

		}
		add_core_of = BRB

	}

	provinces={
		516 6598 11419 13068 
	}
	manpower=359575
	buildings_max_level_factor=1.000
	state_category=large_city
}
