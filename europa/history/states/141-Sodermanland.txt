state={
	id=141
	name="STATE_141"
	resources={
		oil=10
		steel=40
		chromium=23
	}

	history={
		owner = SWE
		victory_points = {
			6050 15 
		}
		victory_points = {
			11197 5 
		}
		victory_points = {
			9209 3 
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 1
			6050 = {
				naval_base = 3
			}
		}
		add_core_of = SWE
	}

	provinces={
		35 114 161 3106 3182 6050 6084 6127 6164 6188 6209 9109 9149 9185 9209 11044 11082 11163 11188 11197 11278 13097 13098 
	}
	manpower=153846
	buildings_max_level_factor=1.000
	state_category=large_town
}
