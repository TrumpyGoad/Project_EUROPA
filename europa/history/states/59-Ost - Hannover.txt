
state={
	id=59
	name="STATE_59"
	resources={
		aluminium=20
		steel=40
		chromium=40
	}

	history={
		owner = BRU
		victory_points = {
			11402 10 
		}
		victory_points = {
			6377 10 
		}
		victory_points = {
			3234 3 
		}
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 2

		}
		add_core_of = BRU

	}

	provinces={
		247 405 526 3234 3395 3561 6377 6513 11402 11493 
	}
	manpower=412717
	buildings_max_level_factor=1.000
	state_category=city
}
