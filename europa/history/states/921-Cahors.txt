state={
	id=921
	name="STATE_921"
	resources={
		chromium=12
	}

	history={
		owner = FRA
		victory_points = {
			752 10 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			arms_factory = 1

		}
		add_core_of = FRA
		add_core_of = GUY
		add_core_of = TOU

	}

	provinces={
		752 755 3751 3948 6775 9734 11702 11718 
	}
	manpower=369347
	buildings_max_level_factor=1.000
	state_category=large_town
}
