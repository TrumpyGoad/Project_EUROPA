state={
	id=788
	name="STATE_788"
	resources={
		oil=4
	}

	history={
		owner = EFR
		add_core_of = EFR
		victory_points={
				11360 5
			}
		buildings={
			infrastructure = 2
			11360={
				naval_base = 1
			}
		}
	}

	provinces={
		11360 
	}
	manpower=103179
	buildings_max_level_factor=1.000
	state_category=rural
}
