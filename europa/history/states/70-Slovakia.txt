
state={
	id=70
	name="STATE_70"
	resources={
		oil=13
		tungsten=40
	}

	history={
		owner = HUN
		victory_points = {
			9692 15 
		}
		victory_points = {
			11679 5 
		}
		victory_points = {
			555 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			arms_factory = 1

		}
		add_core_of = HUN
		add_core_of = SLO

	}

	provinces={
		541 555 3537 3716 6561 9537 9692 11522 11679 
	}
	manpower=527241
	buildings_max_level_factor=1.000
	state_category=large_city
}
