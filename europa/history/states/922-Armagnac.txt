state={
	id=922
	name="STATE_922"
	resources={
		aluminium=45
		chromium=28
	}

	history={
		owner = FRA
		victory_points = {
			929 15 
		}
		victory_points = {
			9884 10 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			arms_factory = 1

		}
		add_core_of = FRA
		add_core_of = ENG
		add_core_of = AMG
		add_core_of = FOI

	}

	provinces={
		929 3740 3749 6759 9882 9884 11697 11699 12169 
	}
	manpower=474874
	buildings_max_level_factor=1.000
	state_category=city
}
