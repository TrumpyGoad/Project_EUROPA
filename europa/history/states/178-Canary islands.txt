
state={
	id=178
	name="STATE_178"
	manpower = 96457
	
	state_category = rural

	history={
		owner = CST
		victory_points = {
			13071 5 
		}
		buildings = {
			infrastructure = 2
			13071 = {
				naval_base = 1
			}
		}
		add_core_of = CST
	}

	provinces={
		13071 
	}
}
