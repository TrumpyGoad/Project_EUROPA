state={
	id=81
	name="STATE_81"
	manpower=211640
	resources={
		oil=10
		aluminium=20
	}
	
	state_category = town
	
	history={
		owner = WAL
		victory_points = {
			6652 10 
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = WAL
	}
	provinces={
		637 3630 3645 6652 6667 11588 11605 11657 
	}
}
