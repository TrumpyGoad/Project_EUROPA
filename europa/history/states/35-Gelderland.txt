
state={
	id=35
	name="STATE_35"
	resources={
		aluminium=55
		chromium=44
	}

	history={
		owner = BRB
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 2

		}
		victory_points = {
			11562 15 
		}
		victory_points = {
			6496 20 
		}
		add_core_of = BRB

	}

	provinces={
		3262 6496 6500 11456 11562 
	}
	manpower=313475
	buildings_max_level_factor=1.000
	state_category=large_city
}
