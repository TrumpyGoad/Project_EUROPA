﻿division_template = {
	name = "Infantry Regiment"
	division_names_group = KOR_INF_01

	regiments = {
		pikemen = { x = 0 y = 0 }
		arbalests = { x = 0 y = 1 }
		pikemen = { x = 1 y = 0 }
		archers = { x = 1 y = 1 }
	}
	support = {
		arbalests_support = { x = 0 y = 0 }
		archers_support = { x = 0 y = 1 } 
	}
}
division_template = {
	name = "Cavalry Regiment"
	division_names_group = KOR_CAV_01

	regiments = {
		spear_cav = { x = 0 y = 0 }
		heavy_lancers = { x = 0 y = 1 }
		spear_cav = { x = 1 y = 0 }
		cav_archers = { x = 1 y = 1 }
	}
}
units = {
	#HANSEONG#
	division= {	
		name = "Infantry Regiment"
		location = 7125
		division_template = "Infantry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
	division= {	
		name = "Infantry Regiment"
		location = 7125
		division_template = "Infantry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
	division= {	
		name = "Infantry Regiment"
		location = 7125
		division_template = "Infantry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
	division= {	
		name = "Cavalry Regiment"
		location = 7125
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
	division= {	
		name = "Cavalry Regiment"
		location = 7125
		division_template = "Cavalry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
}