﻿division_template = {
	name = "Infantry Regiment"
	division_names_group = BAD_INF_01

	regiments = {
		pikemen = { x = 0 y = 0 }
		halberdiers = { x = 0 y = 1 }
		pikemen = { x = 1 y = 0 }
		arquebusiers = { x = 1 y = 1 }
	}
	support = {
		arquebusiers_support = { x = 0 y = 0 }
		arbalests_support = { x = 0 y = 1 } 
	}
}
division_template = {
	name = "Cavalry Regiment"
	division_names_group = BAD_CAV_01

	regiments = {
		spear_cav = { x = 0 y = 0 }
		heavy_lancers = { x = 0 y = 1 }
		spear_cav = { x = 1 y = 0 }
		heavy_lancers = { x = 1 y = 1 }
	}
}
units = {
	#BADEN
	division= {	
		name = "Infantry Regiment"
		location = 6542
		division_template = "Infantry Regiment"
		start_experience_factor = 0.1
		start_equipment_factor = 1

	}
}